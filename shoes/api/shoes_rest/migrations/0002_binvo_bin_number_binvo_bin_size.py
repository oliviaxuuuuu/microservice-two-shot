# Generated by Django 4.0.3 on 2023-01-20 01:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shoes_rest', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='binvo',
            name='bin_number',
            field=models.PositiveIntegerField(default=1),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='binvo',
            name='bin_size',
            field=models.PositiveIntegerField(default=1),
            preserve_default=False,
        ),
    ]
