import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "shoes_project.settings")
django.setup()



# Import models from hats_rest, here.
# from shoes_rest.models import Something

from shoes_rest.models import BinVO;

def poll():
    while True:
        print('Shoes poller polling for data')
        try:
            response = requests.get("http://wardrobe-api:8000/api/bins/")
            content = json.loads(response.content)

            for bin in content["bins"]:
                BinVO.objects.update_or_create(
                    import_href=bin["href"],
                    defaults={
                        # "id": bin["id"],
                        "closet_name": bin["closet_name"],
                        "bin_number": bin["bin_number"],
                        "bin_size": bin["bin_size"],
                    },
                )
            print(BinVO.objects.all())
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)

# x=BinVO.objects.all()
# # print(x[0].id)
# print(x[1].id)
# print(x[1].import_href)
# print(x[1].closet_name)
# print(x[1].bin_number)
# print(x[1].bin_size)


if __name__ == "__main__":
    poll()
