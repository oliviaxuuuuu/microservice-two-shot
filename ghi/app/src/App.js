import MainPage from './MainPage';
import Nav from './Nav';
import ShoesList from './ShoesList';
import ShoeForm from './ShoeForm';
import { BrowserRouter,Routes,Route } from "react-router-dom";
import React, { useEffect, useState} from 'react'

function App() {
  const [shoes,setShoes] = useState([]);

  const getShoes = async () => {
    const response = await fetch (`http://localhost:8080/shoes`);
    console.log(response);
    if (response.ok){
      const data = await response.json();
      const shoes = data.shoes;
      setShoes(shoes);
    }
    else{
      console.error(response);
    }
  }

  useEffect(() => {
    getShoes();
  },[])

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="shoes">
            <Route path="" element={<ShoesList shoes={shoes} getShoes={getShoes} />} />
            <Route path="new" element={<ShoeForm getShoes={getShoes}/>} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
