import React, {useEffect, useState} from 'react'

function ShoeForm() {
    const [bins,setBins] = useState([]);
    const [manufacturer, setManufacturer] = useState('');
    const [modelName, setModelName] = useState('');
    const [color, setColor] = useState('');
    const [picture_url, setPicture_url] = useState("");
    const [bin, setBin] = useState("");

    const handleChangeManufucturer = (event) => {
        setManufacturer(event.target.value);
    }
    const handleChangeModelName = (event) => {
        setModelName(event.target.value);
    }
    const handleChangeColor= (event) => {
        setColor(event.target.value);
    }
    const handleChangePicture= (event) => {
        setPicture_url(event.target.value);
    }
    const handleChangeBin= (event) => {
        setBin(event.target.value);
    }

    const handleSubmit = async (event) => {
      event.preventDefault();
      const data = {};
      data.manufacturer = manufacturer;
      data.model_name = modelName;
      data.color = color;
      data.picture_url = picture_url;

      data.bin = bin;

      const shoeUrl = 'http://localhost:8080/shoes/';
      const fetchOptions = {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
          'Content-Type': 'application/json',
        },
      };

      const shoeResponse = await fetch(shoeUrl, fetchOptions);
      if (shoeResponse.ok) {
        const newShoe = await shoeResponse.json();
        setManufacturer("");
        setModelName("");
        setColor("");
        setPicture_url("");
        setBin("");
      }
    }

    const fetchData = async () => {
        const binUrl = "http://localhost:8100/api/bins/";
        const binResponse = await fetch(binUrl);

        if (binResponse.ok){
            const data = await binResponse.json();
            setBins(data.bins);
        }
    }

    useEffect(() => {fetchData();}, []);

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new shoe</h1>
                    <form onSubmit={handleSubmit} id="create-shoe-form">
                        <div className="form-floating mb-3">
                        <input onChange={handleChangeManufucturer} value={manufacturer} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control"/>
                        <label htmlFor="manufacturer">Manufacturer</label>
                        </div>
                        <div className="form-floating mb-3">
                        <input onChange={handleChangeModelName}  placeholder="Model Name" required type="text" name="model_name" id="model_name" className="form-control"/>
                        <label htmlFor="model_name">Model Name</label>
                        </div>
                        <div className="form-floating mb-3">
                        <input onChange={handleChangeColor} placeholder="Color" required type="text" name="color" id="color" className="form-control"/>
                        <label htmlFor="color">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                        <input onChange={handleChangePicture} placeholder="Picture Url" required type="text" name="picture_url" id="picture_url" className="form-control"/>
                        <label htmlFor="picture_url">Picture Url</label>
                        </div>
                        <div className="mb-3">
                        <select onChange={handleChangeBin} required name="bin" id="bin" className="form-select">
                            <option  value="">Choose a bin</option>
                            {bins.map(bin => {
                                return (
                                    <option key={bin.href} value={bin.href}>
                                    {bin.closet_name}
                                    </option>
                                );
                            })}
                        </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
         </div>
    )
}
export default ShoeForm;