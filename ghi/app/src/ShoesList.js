import React, { useEffect, useState } from 'react';
import { HashRouter } from 'react-router-dom';

function ShoesList({ shoes, getShoes}) {

    async function deleteShoe(id) {
        const url=`http://localhost:8080/shoes/${id}`;
        const fetchConfig ={
            method: 'DELETE',
        }
        const response = await fetch(url, fetchConfig)
        return getShoes(response);
    }

    return (
        <table class="table table-striped">
        <thead>
          <tr>
            <th>Manufacturer</th>
            <th>Model Name</th>
            <th>Color</th>
            <th>Picture</th>
            <th>Closet Name</th>
            <th>Bin Number</th>
            <th>Bin Size</th>
            <th>Delete This</th>
          </tr>
        </thead>
        <tbody>
          {shoes.map((shoe) => {
            return (
              <tr key={shoe.href}>
                <td>{ shoe.manufacturer }</td>
                <td>{ shoe.model_name }</td>
                <td>{ shoe.color }</td>
                <td>
                <img
                    src={shoe.picture_url}
                    width="200px"
                    height="200px"
                    className="img-thumbnail shoes"/>
                </td>
                <td>{ shoe.closet_name }</td>
                <td>{ shoe.bin_number }</td>
                <td>{ shoe.bin_size }</td>
                <td>
                    <button type="button" value={shoe.id} onClick={() => deleteShoe(shoe.id)}>Delete</button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  }

export default ShoesList;